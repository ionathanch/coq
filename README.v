(**  Is Sized Typing for Coq Practical?
  *
  *  You will need your environment set up to build Coq. Instructions are provided
  *  in the Coq repository, but they are generally as follows:
  *    1. `sudo apt install opam` (OCaml package manager) (for Ubuntu)
  *    2. `opam init && opam switch create ocaml-base-compiler` (OCaml compiler)
  *    3. `opam install num ocamlfind` (OCaml libraries needed for compilation)
  *
  *  To use the Coq toplevel (REPL) and compiler/checker locally, in `coq/`, run:
  *    1. `./configure -local` (N.B. this turns warnings into errors)
  *    2. `make coqbinaries` (for native code) or `make byte` (for bytecode)
  *    3. `make coqlib` (stdlib) or `make init` (core library only)
  *    4. `bin/coqtop` (if native code) or `bin/coqtop.byte` (if bytecode)
  *    5. `bin/coqc` (if native code) or `bin/coqc.byte` (if bytecode)
  *
  *  The `Guard Checking` flag toggles the existing guard-predicate–based checking,
  *  while the `Sized Typing` flag toggles our new CIC^* size inference/checking.
  *
  *  More example programs can be found in `coq/test-suite/success/sized_typing.v`.
  *  Some programs mentioned in the paper are included below.
  *
  *  In our evaluation, we run `coq/bin/coqc` comparing `coq/theories/MSets/MSetList.v`
  *  against the same file with sized typing and profiling on in `MSetList_sized.v`.
  *  The raw data are in `MSetList/` and `MSetList_sized/`, and the statistical
  *  analysis is done in a Jupyter notebook at `analysis.ipynb`.
  *)

Unset Guard Checking.
Set Sized Typing.

(** Arithmetic *)

Reserved Infix "+" (at level 50, left associativity).
Reserved Infix "-" (at level 50, left associativity).
Reserved Infix "/" (at level 40, left associativity).

Fixpoint plus n m : nat :=
  match n with
  | O => m
  | S p => S (plus p m)
  end
where "n + m" := (plus n m).

Fixpoint minus n m :=
  match n, m with
  | O, _ => n
  | _, O => n
  | S n', S m' =>
    minus n' m'
  end
where "n - m" := (minus n m).

Fixpoint div n m :=
  match n with
  | O => O
  | S n' => S (div (minus n' m) m)
  end
where "n / m" := (div n m).

(** Quicksort *)

Fixpoint leb n m :=
  match n, m with
    | O, _ => true
    | _, O => false
    | S n', S m' => leb n' m'
  end.

Definition gtb n m :=
  negb (leb n m).

Fixpoint filter {T} (p: T -> bool) (l: list T) :=
  match l with
  | nil => nil
  | cons hd tl =>
    if (p hd) then
      cons hd (filter p tl)
    else
      filter p tl
  end.

Fixpoint append {T} (l1 l2: list T) :=
  match l1 with
  | nil => l2
  | cons hd tl =>
    cons hd (append tl l2)
  end.

Fixpoint quicksort (l: list nat) :=
  match l with
  | nil => nil
  | cons hd tl => append
    (quicksort (filter (gtb hd) tl))
    (cons hd (quicksort (filter (leb hd) tl)))
  end.

(** GCD
  These are lifted directly from Nat in the standard library.
  See also https://github.com/coq/coq/wiki/CoqTerminationDiscussion#sized-types. *)

Fixpoint divmod n m q u :=
  match n with
  | O => (q, u)
  | S n' =>
    match u with
      | 0 => divmod n' m (S q) m
      | S u' => divmod n' m q u'
    end
  end.

Definition modulo n m :=
  match m with
  | O => n
  | S m' => m' - snd (divmod n m' O m')
  end.

Infix "%" := modulo (at level 40, no associativity).

Fail Fixpoint gcd n m :=
  match n with
   | O => m
   | S n' => gcd (m % (S n')) (S n')
  end.

Set Guard Checking.

Fixpoint gcd n m :=
  match n with
   | O => m
   | S n' => gcd (m % (S n')) (S n')
  end.

Unset Guard Checking.

(** Other inductives *)

Inductive Vector (A: Type): nat -> Type :=
| VNil: Vector A O
| VCons: forall n, A -> Vector A (S n).

Inductive Box (A: Type): Type :=
| MkBox: A -> Box A.

Inductive N: Type :=
| z: N
| s: Box N -> N.

Definition counterexample :=
  match s (MkBox N z) with
  | z => z
  | s x => (fun A => fun x: A => z) (Box N) x
  end.

(** Coinductives *)

CoInductive Conat: Type :=
| SS: Conat -> Conat.

CoFixpoint omega := SS omega.

CoInductive Stream (A : Type) :=
  Cons : A -> Stream A -> Stream A.

CoFixpoint const A a : Stream A := Cons A a (const A a).

(** Performance degredation
  The following definitions show how size variables can grow exponentially,
  which leads to a corresponding exponential growth in type checking time.
  The sixth definition is commented out because it is /very/ slow. *)

Time Definition nats1  := (nat, nat, nat, nat, nat, nat, nat, nat).
Time Definition nats2  := (nats1, nats1, nats1, nats1).
Time Definition nats3  := (nats2, nats2, nats2, nats2).
Time Definition nats4  := (nats3, nats3, nats3, nats3).
Time Definition nats5  := (nats4, nats4, nats4, nats4).
(* Time Definition nats6  := (nats5, nats5, nats5, nats5). *)

(** Unsized pretyping ⇒ fixpoint typing failture
  See https://github.com/ionathanch/coq/issues/3. *)

Fail Definition test :=
  let id (x : nat) := x in
  fix f (n : nat) :=
    match n with
    | O => O
    | S k => f (id k)
    end.

(** STLC
  A model of capture-avoiding substitution in simply-typed lambda calculus.
  You may need to run `make theories/Strings/String.vo` first.

  Notice that `freshen` is size-preserving, which is why it can be used in
  the recursive argument of `subst`, but `subst` itself is not size-preserving,
  since `val` may be a "larger" term than `exp`. *)

Require Import Strings.String.

Module STLC.

Parameter names: list string.
Parameter fresh: True -> string.

Inductive Typ: Type :=
  | unit: Typ
  | arr (T U: Typ): Typ.

Inductive Exp: Type :=
  | var (x: string): Exp
  | lam (x: string) (T: Typ) (body: Exp): Exp
  | app (e1: Exp) (e2: Exp): Exp.

(* freshen : string^∞ → string^∞ → Exp^υ → Exp^υ *)
Fixpoint freshen (old: string) (new: string) (e: Exp) :=
  match e with
  | var x => if (x =? old)%string then var new else e
  | app e1 e2 => app (freshen old new e1) (freshen old new e2)
  | lam x T body => lam x T (freshen old new body)
  end.

(* subst : string^∞ → Exp^∞ → Exp^υ → Exp^∞ *)
Fixpoint subst (name: string) (val exp: Exp) {struct exp} :=
  match exp with
  | var x => if (x =? name)%string then val else exp
  | app e1 e2 => app (subst name val e1) (subst name val e2)
  | lam x T body =>
    if (x =? name)%string then exp else
    let y := fresh I in
    lam y T (subst name val (freshen x y body))
  end.

End STLC.
