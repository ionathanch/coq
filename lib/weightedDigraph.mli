module type COMPARABLE = sig
  type t
  val compare : t -> t -> int
  val hash : t -> int
  val equal : t -> t -> bool
end

module Make : functor(V: COMPARABLE) -> sig
  type t
  type vertex = V.t

  module E : sig
    type t = vertex * int * vertex
    val compare : t -> t -> int
  end
  type edge = E.t

  module H : sig
    type 'a t = 'a Hashtbl.Make(V).t
    val fold : (V.t -> 'a -> 'c -> 'c) -> 'a t -> 'c -> 'c
  end

  val nb_vertex : t -> int
  val nb_edges : t -> int
  val mem_vertex : t -> vertex -> bool
  val mem_edge_e : t -> edge -> bool
  val create : ?size:int -> unit -> t
  val copy : t -> t
  val add_edge_e : t -> edge -> unit
  val remove_vertex : t -> vertex -> unit
  val find_all_edges : t -> vertex -> vertex -> edge list
  val fold_edges_e : (edge -> 'a -> 'a) -> t -> 'a -> 'a
  val iter_edges_e : (edge -> unit) -> t -> unit
  val succ : t -> vertex -> vertex list
  val pred : t -> vertex -> vertex list
  val all_shortest_paths_nonnegative : t -> vertex -> int H.t
  val all_shortest_paths : t -> vertex -> int H.t
  val find_negative_cycle : t -> edge list
  val components : t -> vertex list array
end
