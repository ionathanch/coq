open Hashset.Combine
open Util

(** Helpers *)

let (<<) f g x = f @@ g x

module Time =
struct
  open Hashtbl

  let table = create 16

  let get name = Option.default 0. @@ find_opt table name

  let write filename pps =
    let open Format in
    let open Pp in
    let oc = open_out_gen [Open_append; Open_creat] 0o666 filename in
    let fmt = formatter_of_out_channel oc in
    pp_with fmt @@ seq pps;
    pp_print_flush fmt ();
    close_out oc

  let elapse_and_write filename f pps =
    let start = Sys.time () in
    try
      let result = f () in
      let fin = Sys.time () in
      write filename (pps (fin -. start));
      result
    with e ->
      let fin = Sys.time () in
      write filename (pps (fin -. start));
      raise e

  let elapse_collect name ?(without = []) f =
    let without_time_start = List.fold_left (fun t name -> t +. get name) 0. without in
    let start = Sys.time () in
    let result = f () in
    let fin = Sys.time () in
    let without_time_fin = List.fold_left (fun t name -> t +. get name) 0. without in
    replace table name (get name +. (fin -. start) -. (without_time_fin -. without_time_start));
    result

  let write_collect ?(filename = "stats.csv") () =
    let open Pp in
    let oc = open_out_gen [Open_append; Open_creat] 0o666 filename in
    let fmt = Format.formatter_of_out_channel oc in
    let writer name time =
      pp_with fmt @@ seq [str name; str ","; real time; fnl ()];
      Format.pp_print_flush fmt ();
      flush oc in
    iter writer table;
    close_out oc

  let clear () = clear table
end

(** Size variables *)

module SVar =
struct
  type t = int
  let equal = Int.equal
  let compare = Int.compare
  let succ = succ
  let skip = (+)
  let infty = -1 (* For constraint representation only!!! *)
  let is_infty = equal infty
  let pr var =
    let open Pp in
    str "s" ++ int var
end

(** Collections of size variables *)

module SVars =
struct
  include Int.Set

  let union_list = List.fold_left union empty

  let pr vars =
    let open Pp in
    let pr_var v = str "s" ++ int v in
    seq [str "{"; pr_enum pr_var (elements vars); str "}"]
end

(** Sizes, for sized annotations *)

module Size =
struct
  type t = Infty | SizeVar of SVar.t * int

  let mk var size = SizeVar (var, size)

  let hat a = match a with
  | SizeVar (var, sz) -> SizeVar (var, succ sz)
  | _ -> a

  let compare s1 s2 =
    match s1, s2 with
    | Infty, Infty -> 0
    | Infty, _     -> 1
    | _, Infty     -> -1
    | SizeVar (var1, sz1), SizeVar (var2, sz2) ->
      let nc = Int.compare var1 var2 in
      if not (Int.equal nc 0) then nc
      else Int.compare sz1 sz2

  let equal s1 s2 = Int.equal 0 @@ compare s1 s2

  (* Substitute the size variable of [sof] by size expression [sby] *)
  let subst sof sby =
    if sof == sby || equal sof sby then sof else
    match sof with
    | Infty -> sof
    | SizeVar (_, size) ->
      if Int.equal size 0 then sby else
      match sby with
      | Infty -> sby
      | SizeVar (svar, size') -> SizeVar (svar, size + size')

  let pr s =
    let open Pp in
    match s with
    | Infty -> str "∞"
    | SizeVar (s, n) ->
      if Int.equal n 0 then SVar.pr s else
      seq [SVar.pr s; str "+"; int n]

  let show a = Pp.string_of_ppcmds (pr a)

  let hash a =
    match a with
    | Infty -> combine 1 (show a |> String.hash)
    | SizeVar (n, i) -> combine3 2 (Int.hash n) (Int.hash i)
end

(** Annotations, attached to (co)inductive types *)

module Annot =
struct
  open Size

  type t =
    | Empty (* Bare types with no annotations, input to inference *)
    | Star (* Marks the positions of the (co)recursive types in (co)fixpoints *)
    | Size of Size.t (* Sized types *)

  let infty = Size Infty

  let mk var size = Size (Size.mk var size)

  let hat a =
    match a with
    | Size size -> Size (hat size)
    | _ -> a

  let compare a1 a2 =
    match a1, a2 with
    | Empty, Empty -> 0
    | Empty, _ -> -1 | _, Empty -> 1
    | Star, Star   -> 0
    | Star, _  -> -1 | _, Star  -> 1
    | Size s1, Size s2 -> Size.compare s1 s2

  let equal a1 a2 = Int.equal 0 @@ compare a1 a2

  let sizevar_opt = function
    | Size (SizeVar (var, _)) -> Some var
    | _ -> None

  let pr a =
    let open Pp in
    match a with
    | Empty -> mt ()
    | Star  -> str "*"
    | Size s -> Size.pr s

  let show a = Pp.string_of_ppcmds (pr a)

  let hash a =
    match a with
    | Empty -> combine 1 (show a |> String.hash)
    | Star  -> combine 2 (show a |> String.hash)
    | Size s -> combine 3 (Size.hash s)
end

(** Size state, keeping track of used size variables *)

module State =
struct
  open SVars
  open Size
  open Annot

  type t = {
    next: SVar.t;
    (* next size variable to be used *)
    vars: SVars.t;
    (* all used size variables *)
    pos_vars: SVars.t;
    (* size variables used to replace star annotations *)
    stack: SVars.t list;
    (* stack of old pos_vars *)
  }

  let init = {
    next = 0;
    vars = empty;
    pos_vars = empty;
    stack = [];
  }

  let push state = { state with
    pos_vars = empty;
    stack = state.pos_vars :: state.stack }
  let pop state =
    let pos_vars, stack = match state.stack with
    | [] -> empty, state.stack
    | hd :: tl -> hd, tl in
    { state with
      pos_vars = SVars.union pos_vars state.pos_vars;
      stack = stack }

  let get_vars state = state.vars
  let get_pos_vars state = state.pos_vars
  let remove_pos_vars rem state =
    { state with pos_vars = diff state.pos_vars rem }

  let next ?s:(s=Empty) state =
    match s with
    | Empty | Size Infty ->
      Annot.mk state.next 0,
      { state with
        next = succ state.next;
        vars = add state.next state.vars }
    | Star ->
      Annot.mk state.next 0,
      { state with
        next = succ state.next;
        vars = add state.next state.vars;
        pos_vars = add state.next state.pos_vars }
    | _ -> (s, state)

  let next_size state =
    Size.mk state.next 0,
    { state with
      next = succ state.next;
      vars = add state.next state.vars }

  let pr state =
    let open Pp in
    let stg_pp = SVar.pr state.next in
    let vars_pp = SVars.pr state.vars in
    let stars_pp = SVars.pr state.pos_vars in
    seq [str"<"; stg_pp; pr_comma (); vars_pp; pr_comma (); stars_pp; str ">"]

  let pop state =
    if !Flags.profiling then
      Time.elapse_collect "state" ~without:[] (fun () -> pop state)
    else pop state

  let remove_pos_vars rem state =
    if !Flags.profiling then
      Time.elapse_collect "state" ~without:[] (fun () -> remove_pos_vars rem state)
    else remove_pos_vars rem state

  let next ?s:(s=Empty) state =
    if !Flags.profiling then
      Time.elapse_collect "state" ~without:[] (fun () -> next ~s state)
    else next ~s state

  let next_size state =
    if !Flags.profiling then
      Time.elapse_collect "state" ~without:[] (fun () -> next_size state)
    else next_size state
end

(** Size variable substitution maps from size variables to sizes *)

module SMap =
struct
  open Size

  module M = Map.Make(SVar)
  type t = Size.t M.t

  let empty = M.empty
  let cardinal = M.cardinal
  let get var smap =
    match M.find_opt var smap with
    | Some s -> s
    | None -> mk var 0
  let add key s smap =
    match M.find_opt key smap with
    | Some _ -> smap
    | None -> M.add key s smap

  let vars smap =
    let f _ size_to svars =
      match size_to with
      | Infty -> svars
      | SizeVar (svar, _) -> SVars.add svar svars in
    M.fold f smap SVars.empty

  let fresheners state svars_type svars_body svars_exclude =
    let f svar_from (state, smap) =
      let size_to, state = State.next_size state in
      state, add svar_from size_to smap in
    let state, smap_type = SVars.fold f (SVars.diff svars_type svars_exclude) (state, empty) in
    let g svar_from (state, smap) =
      let size_to, state =
        match M.find_opt svar_from smap_type with
        | Some size -> size, state
        | None -> State.next_size state in
      state, add svar_from size_to smap in
    let state, smap_body =
      match svars_body with
      | Some svars ->
        let state, smap = SVars.fold g (SVars.diff svars svars_exclude) (state, empty) in
        state, Some smap
      | None -> state, None in
    state, smap_type, smap_body

  let subst smap sof =
    match sof with
    | Infty -> sof
    | SizeVar (svar, size) ->
      let sby = get svar smap in
      if sof == sby || Size.equal sof sby then sof else
      if Int.equal size 0 then sby else
      match sby with
      | Infty -> sby
      | SizeVar (svar', size') -> SizeVar (svar', size + size')

  let infinitize = M.map (fun _ -> Infty)
  let compose smap_outer smap_inner =
    M.map (fun size -> subst smap_outer size) smap_inner

  let pr smap =
    let open Pp in
    let pr_map (v, s) = SVar.pr v ++ str " ↦ " ++ Size.pr s in
    seq [str "{"; pr_enum pr_map (M.bindings smap); str "}"]

  let show a = Pp.string_of_ppcmds (pr a)

  let hash smap =
    M.fold (fun svar_from size_to acc -> combine3 acc svar_from (Size.hash size_to)) smap 0

  let get vars smap =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> get vars smap)
    else get vars smap

  let add key s smap =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> add key s smap)
    else add key s smap

  let vars smap =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> vars smap)
    else vars smap

  let fresheners state svars_type svars_body svars_exclude =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:["state"] (fun () -> fresheners state svars_type svars_body svars_exclude)
    else fresheners state svars_type svars_body svars_exclude

  let subst smap sof =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> subst smap sof)
    else subst smap sof

  let infinitize smap =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> infinitize smap)
    else infinitize smap

  let compose smap_outer smap_inner =
    if !Flags.profiling then
      Time.elapse_collect "smap" ~without:[] (fun () -> compose smap_outer smap_inner)
    else compose smap_outer smap_inner
end

(** Collections of size constraints *)

(* Constraints.t: A weighted, directed graph.
  Given a constraint v1+s1 ⊑ v2+s2, we add an edge
  from v1 to v2 with weight s2 - s1.
  N.B. Infty sizes are stored as (-1) *)
module Constraints =
struct
  open Size
  open Annot

  module S = Set.Make(struct
    module G = WeightedDigraph.Make(Int)
    type t = G.edge
    let compare = G.E.compare
  end)

  type t = S.t
  type 'a constrained = 'a * t
  let mkEdge var1 size var2 = (var1, size, var2)

  let empty () = S.empty
  let union = S.union
  let union_list = List.fold_left union (empty ())
  let add a1 a2 cstrnts =
    begin
    match a1, a2 with
    | Size s1, Size s2 ->
      begin
      match s1, s2 with
      | Infty, Infty -> cstrnts
      | SizeVar (var1, sz1), SizeVar (var2, sz2) ->
        if SVar.equal var1 var2 && sz1 <= sz2 then cstrnts
        else S.add (mkEdge var1 (sz2 - sz1) var2) cstrnts
      | Infty, SizeVar (var, _) ->
        S.add (mkEdge SVar.infty 0 var) cstrnts
      | SizeVar _, Infty -> cstrnts
      end
    | _ -> cstrnts
    end

  let finite_vars cstrnts =
    let f cstrnt svars =
      match cstrnt with
      | (var1, _, var2) when SVar.equal var1 SVar.infty && SVar.equal var2 SVar.infty -> svars
      | (var, _, infty) when SVar.equal infty SVar.infty -> SVars.add var svars
      | (infty, _, var) when SVar.equal infty SVar.infty -> SVars.add var svars
      | (var1, _, var2) -> SVars.add var2 (SVars.add var1 svars) in
    S.fold f cstrnts SVars.empty

  let map smap =
    let f (var1, size, var2) cstrnts =
      let s1 = SMap.get var1 smap in
      let s2 = SMap.get var2 smap in
      match s1, s2 with
      | Infty, Infty -> cstrnts
      | SizeVar (var1, sz1), SizeVar (var2, sz2) ->
        if SVar.equal var1 var2 && sz1 <= sz2 + size then cstrnts
        else S.add (mkEdge var1 (size + sz2 - sz1) var2) cstrnts
      | Infty, SizeVar (var, _) ->
        S.add (mkEdge SVar.infty 0 var) cstrnts
      | SizeVar _, Infty -> cstrnts in
    S.fold f (empty ())

  let pr cstrnts =
    let open Pp in
    let pr_edge (vfrom, wt, vto) =
      let sfrom, sto =
        if wt >= 0
        then SizeVar (vfrom,   0), SizeVar (vto, wt)
        else SizeVar (vfrom, -wt), SizeVar (vto,  0) in
      let sfrom = if SVar.is_infty vfrom then Infty else sfrom in
      let sto   = if SVar.is_infty vto   then Infty else sto   in
      seq [Size.pr sfrom; str "⊑"; Size.pr sto] in
    let pr_graph =
      prlist_with_sep pr_comma identity @@
      S.fold (fun edge prs -> pr_edge edge :: prs) cstrnts [] in
    seq [str "{"; pr_graph; str "}"]

  let union c1 c2 =
    if !Flags.profiling then
      Time.elapse_collect "constraints" ~without:[] (fun () -> union c1 c2)
    else union c1 c2

  let union_list cs =
    if !Flags.profiling then
      Time.elapse_collect "constraints" ~without:[] (fun () -> union_list cs)
    else union_list cs

  let add a1 a2 c =
    if !Flags.profiling then
      Time.elapse_collect "constraints" ~without:[] (fun () -> add a1 a2 c)
    else add a1 a2 c

  let finite_vars c =
    if !Flags.profiling then
      Time.elapse_collect "constraints" ~without:[] (fun () -> finite_vars c)
    else finite_vars c

  let map smap c =
    if !Flags.profiling then
      Time.elapse_collect "constraints" ~without:[] (fun () -> map smap c)
    else map smap c
end

(** RecCheck functions and internal graph representation of constraints *)

module RecCheck =
struct
  open SVars

  module S = Constraints.S
  module G = WeightedDigraph.Make(Int)
  type g = G.t

  let to_graph cstrnts =
    let g = G.create () in
    S.iter (G.add_edge_e g) cstrnts; g
  let of_graph g =
    G.fold_edges_e S.add g S.empty

  (* N.B. [insert] and [remove] are mutating functions!! *)
  let insert g vfrom wt vto =
    G.add_edge_e g (Constraints.mkEdge vfrom wt vto)
  let remove g s =
    G.remove_vertex g s
  let contains g vfrom vto =
    not @@ List.is_empty @@ G.find_all_edges g vfrom vto

  let sup g s =
    if G.mem_vertex g s
    then SVars.of_list @@ G.succ g s
    else SVars.empty
  let sub g s =
    if G.mem_vertex g s
    then SVars.of_list @@ G.pred g s
    else SVars.empty

  let find_negative_cycle_vars g =
    let edges = G.find_negative_cycle g in
    List.fold_left (fun vars (vfrom, _, vto) ->
      SVars.add vfrom @@ SVars.add vto vars) SVars.empty edges

  exception RecCheckFailed of Constraints.t * SVars.t * SVars.t

  (* We could use ocamlgraph's Fixpoint module to compute closures
    but their implementation is very wordy and this suffices *)
  let closure get_adj cstrnts init =
    let rec closure_rec init fin =
      match choose_opt init with
      | None -> fin
      | Some s ->
        let init_rest = SVars.remove s init in
        if mem s fin
        then closure_rec init_rest fin
        else
          let init_new = get_adj cstrnts s in
          closure_rec (union init_rest init_new) (add s fin) in
    filter (not << SVar.is_infty) (closure_rec init empty)

  (* ⊓ (downward closure), ⊔ (upward closure) *)
  let downward = closure sub
  let upward = closure sup

  let insert_from_set var_sub cstrnts =
    iter (fun var_sup -> insert cstrnts var_sub 0 var_sup)
  let remove_from_set cstrnts =
    iter (fun var ->
      remove cstrnts var)

  let rec remove_neg cstrnts vneg =
    let neg_cycle_vars = find_negative_cycle_vars cstrnts in
    let vneg' = upward cstrnts neg_cycle_vars in
    if not (is_empty vneg') then begin
      remove_from_set cstrnts vneg';
      insert_from_set SVar.infty cstrnts vneg';
      remove_neg cstrnts (union vneg vneg')
    end else vneg
  let remove_neg cstrnts = remove_neg cstrnts empty

  let rec_check tau vstar vneq cstrnts =
    let cstrnts' = to_graph cstrnts in

    (** Step 1: Vi = ⊓V*; add τ ⊑ Vi *)
    let vi = downward cstrnts' vstar in
    let () = insert_from_set tau cstrnts' vi in

    (** Step 2, 3: Find, remove negative cycles *)
    let () = ignore @@ remove_neg cstrnts' in

    (** Step 4: Add ∞ ⊑ ⊔V≠ ∩ ⊔Vi *)
    let vi_up = upward cstrnts' vi in
    let vneq_up = upward cstrnts' vneq in
    let vinter = inter vneq_up vi_up in
    let () = insert_from_set SVar.infty cstrnts' vinter in

    (** Step 5: Check ⊔{∞} ∩ Vi = ∅ *)
    let vinf = upward cstrnts' (singleton SVar.infty) in
    let vnull = inter vinf vi in
    if is_empty vnull then of_graph cstrnts'
    else raise (RecCheckFailed (cstrnts, vinf, vi))

  let rec_check tau vstar vneq cstrnts =
    if !Flags.profiling then
      let pps time =
        let open Pp in
        let cstrnts' = to_graph cstrnts in
        [real time; str ","; int (G.nb_edges cstrnts'); str ","; int (G.nb_vertex cstrnts'); str ","; fnl ()] in
      Time.elapse_and_write "reccheck.csv" (fun () -> rec_check tau vstar vneq cstrnts) pps
    else rec_check tau vstar vneq cstrnts

  let solve cstrnts state =
    let cstrnts = to_graph cstrnts in

    (* Remove all size variables that must be mapped to infty
      and map them to infty in the substitution map *)
    let vneg = remove_neg cstrnts in
    let vinf = upward cstrnts (singleton SVar.infty) in
    let () = iter (fun var -> remove cstrnts var) vinf in
    let smap = SVars.fold (fun var smap -> SMap.add var Size.Infty smap) (union vneg vinf) SMap.empty in

    (* Find a valid substitution map for a given connected component *)
    let solve (state, smap) component =
      let open State in
      let base = state.next in
      let state = { state with next = succ state.next } in

      (* The shortest path of weight w from vfrom to all vtos is the constraint {vfrom ⊑ vto + w}.
        Therefore, we want to map each vto to (vfrom - w). However, hats are never negative,
        so we add to each the maximum weight wmax: [vto ↦ vfrom + wmax - w].
        This has the added bonus of guaranteeing a "minimal" solution in that
        no annotation can have a fewer number of hats than (wmax - w). *)
      let () = List.iter (fun vto -> insert cstrnts base 0 vto) component in
      let shortest_paths = G.all_shortest_paths cstrnts base in
      let wmax = G.H.fold (fun _ w' w -> max w w') shortest_paths min_int in
      let smap = G.H.fold (fun var w smap -> SMap.add var (Size.mk base (wmax - w)) smap) shortest_paths smap in
    state, smap in

    (* Get all the connected components of the graph
      and deal with them one at a time *)
    let components = G.components cstrnts in
    Array.fold_left solve (state, smap) components

  let solve cstrnts state =
    if !Flags.profiling then
      let pps time =
        let open Pp in
        let cstrnts' = to_graph cstrnts in
        [real time; str ","; int (G.nb_edges cstrnts'); str ","; int (G.nb_vertex cstrnts'); str ","; fnl ()] in
      Time.elapse_and_write "solve.csv" (fun () -> solve cstrnts state) pps
    else solve cstrnts state
end
